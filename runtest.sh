#!/bin/bash

function testerror() {
    echo "testerror:" "$@"
}

invalidkey="invalid_key_jd83hd"
gpgdo="gpgdo/gpgdo.py"

python3 "${gpgdo}" -r "${invalidkey}" -- false
[[ $? != 1 ]] && testerror "0"

python3 "${gpgdo}" -r "${invalidkey}" -- true
[[ $? != 0 ]] && testerror "1"

python3 "${gpgdo}" -- true >/dev/null
[[ $? != 0 ]] && testerror "2"

# Test argument error. -r option is missing.
python3 "${gpgdo}" edit non-existent-file-xxx.gpg >/dev/null
[[ $? != 20 ]] && testerror "3"

# Test run error. The binary does not exist.
python3 "${gpgdo}" ./non-existent-binary-xxx >/dev/null
[[ $? != 24 ]] && testerror "4"

exit 0
