all:

install:
	python3 setup.py install

test:
	flake8
	./runtest.sh

check:	test
